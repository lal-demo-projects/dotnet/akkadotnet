﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Akka.Actor;
using AkkaMovieStreamTest.Common.Actors;
using AkkaMovieStreamTest.Common.Messages;

namespace AkkaMovieStreamTest.Remote
{
    class Program
    {
        static ActorSystem actorSystem;
        static void Main(string[] args)
        {
            MainAsync(args).Wait();
        }
        static async Task MainAsync(string[] args)
        {
            actorSystem = ActorSystem.Create("MovieActorSystem");
            Console.WriteLine("Actor system Created");

            //Test3();

            Console.WriteLine("Press enter to stop");
            Console.ReadLine();
            Console.WriteLine("Terminating actor system");
            await actorSystem.Terminate();
            Console.WriteLine("Press enter to exit");
            Console.ReadLine();
        }
    }
}
