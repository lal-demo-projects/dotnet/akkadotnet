﻿
namespace AkkaMovieStreamTest.Common.Messages
{
    public class StopMovieMessage
    {
        public int UserID { get; private set; }
        public StopMovieMessage() { }
        public StopMovieMessage(int UserID)
        {
            this.UserID = UserID;
        }
    }
}
