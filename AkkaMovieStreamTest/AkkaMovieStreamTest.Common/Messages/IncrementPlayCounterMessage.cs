﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AkkaMovieStreamTest.Common.Messages
{
    public class IncrementPlayCounterMessage
    {
        public string MovieName { get; private set; }

        public IncrementPlayCounterMessage(string MovieName)
        {
            this.MovieName = MovieName;
        }
    }
}
