﻿
namespace AkkaMovieStreamTest.Common.Messages
{
    public class PlayMovieMessage
    {
        public int UserID { get; private set; }
        public string Name { get; private set; }
        public int Rating { get; private set; }

        public PlayMovieMessage(string Name, int Rating)
        {
            this.Name = Name;
            this.Rating = Rating;
        }

        public PlayMovieMessage(int UserID, string Name, int Rating)
        {
            this.UserID = UserID;
            this.Name = Name;
            this.Rating = Rating;
        }
    }
}
