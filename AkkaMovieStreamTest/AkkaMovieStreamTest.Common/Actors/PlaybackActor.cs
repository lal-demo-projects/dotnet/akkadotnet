﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Akka.Actor;
using AkkaMovieStreamTest.Common.Messages;

namespace AkkaMovieStreamTest.Common.Actors
{
    public class PlaybackActor : UntypedActor
    {
        public PlaybackActor()
        {
            Console.WriteLine("Creating playback actor");
        }

        protected override void OnReceive(object message)
        {
            if (message is PlayMovieMessage movie)
            {
                Console.WriteLine("Movie Title : " + movie.Name + " Rating " + movie.Rating + "/5");
            }
            else if (message is string)
            {
                Console.WriteLine("Movie Title : " + message);
            }
            else if (message is int)
            {
                Console.WriteLine("Movie Title : " + message + " Rating ?/5");
            }
            else
            {
                Unhandled(message);
            }
        }

        protected override void PostStop()
        {
            base.PostStop();
            Console.WriteLine("Playback Actor PostStop");
        }
    }
}
