﻿using System;
using Akka.Actor;
using AkkaMovieStreamTest.Common.Messages;

namespace AkkaMovieStreamTest.Common.Actors
{
    public class UserActor : LogActorBase
    {
        private int userId = 0;
        private string _currentlyWatching;

        public UserActor()
        {
            Console.WriteLine("Creating User Actor");
            //Receive<PlayMovieMessage>(movie => HandlePlayMovie(movie.Name));
            //Receive<StopMovieMessage>(movie => HandleStopMovie());

            Console.WriteLine("Setting initial behavior");
            Stopped();
        }

        public UserActor(int userId)
        {
            this.userId = userId;
            Console.WriteLine("Creating User Actor");
            //Receive<PlayMovieMessage>(movie => HandlePlayMovie(movie.Name));
            //Receive<StopMovieMessage>(movie => HandleStopMovie());

            Console.WriteLine("Setting initial behavior");
            Stopped();
        }

        private void Playing()
        {
            Receive<PlayMovieMessage>(movie => Console.WriteLine("Error : Cant play movie while playing another"));
            Receive<StopMovieMessage>(movie => StopMovie());
        }

        private void Stopped()
        {
            Receive<PlayMovieMessage>(movie => PlayMovie(movie.Name));
            Receive<StopMovieMessage>(movie => Console.WriteLine("Error : Cant stop movie without playing"));
        }

        private void PlayMovie(string title)
        {
            _currentlyWatching = title;
            Console.WriteLine("Playing movie " + title);
            Context.ActorSelection("/user/PlaybackActorV3/PlaybackStatistics/MoviePlayCounter")
                .Tell(new IncrementPlayCounterMessage(title));
            Become(Playing);
        }

        private void StopMovie()
        {
            _currentlyWatching = null;
            Console.WriteLine("Playback stopped");
            Become(Stopped);
        }
    }
}
