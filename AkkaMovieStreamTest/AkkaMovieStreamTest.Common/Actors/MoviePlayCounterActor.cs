﻿using System;
using System.Collections.Generic;
using Akka.Actor;
using AkkaMovieStreamTest.Common.Messages;

namespace AkkaMovieStreamTest.Common.Actors
{
    public class MoviePlayCounterActor : LogActorBase
    {
        private readonly Dictionary<string, int> movieCounts;
        public MoviePlayCounterActor()
        {
            movieCounts = new Dictionary<string, int>();
            Receive<IncrementPlayCounterMessage>(message => HandleIncrementMessage(message));
        }

        private void HandleIncrementMessage(IncrementPlayCounterMessage message)
        {
            if (message.MovieName == "300") throw new NotSupportedException("Can't process 300");
            if (movieCounts.ContainsKey(message.MovieName))
            {
                if (movieCounts[message.MovieName] > 1) throw new NotImplementedException("Movie watched more than 2 times");
                movieCounts[message.MovieName]++;
            }
            else
            {
                movieCounts.Add(message.MovieName, 1);
            }
            Console.WriteLine($"Movie {message.MovieName} played {movieCounts[message.MovieName]} time(s)");
        }
    }
}
