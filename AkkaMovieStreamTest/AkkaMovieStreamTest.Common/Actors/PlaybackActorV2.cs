﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Akka.Actor;
using AkkaMovieStreamTest.Common.Messages;

namespace AkkaMovieStreamTest.Common.Actors
{
    public class PlaybackActorV2 : LogActorBase
    {
        public PlaybackActorV2()
        {
            Console.WriteLine("Creating playback v2 actor");
            Receive<PlayMovieMessage>(movie => Console.WriteLine("Movie Title : " + movie.Name + " Rating " + movie.Rating + "/5"));
            Receive<string>(message => Console.WriteLine("Movie Title : " + message));
            Receive<int>(message => Console.WriteLine("Movie Title : " + message + " Rating ?/5"));
        }
    }
}
