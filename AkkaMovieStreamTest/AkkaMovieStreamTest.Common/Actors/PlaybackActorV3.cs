﻿using System;
using Akka.Actor;
using AkkaMovieStreamTest.Common.Messages;

namespace AkkaMovieStreamTest.Common.Actors
{
    public class PlaybackActorV3 : LogActorBase
    {
        public PlaybackActorV3()
        {
            Console.WriteLine("Creating playback v3 actor");
            Context.ActorOf(Props.Create<UserCoordinatorActor>(), "UserCoordinator");
            Context.ActorOf(Props.Create<PlaybackStatisticsActor>(), "PlaybackStatistics");

            Receive<PlayMovieMessage>(movie => Console.WriteLine("Movie Title : " + movie.Name + " Rating " + movie.Rating + "/5"));
            Receive<string>(message => Console.WriteLine("Movie Title : " + message));
            Receive<int>(message => Console.WriteLine("Movie Title : " + message + " Rating ?/5"));
        }
    }
}
