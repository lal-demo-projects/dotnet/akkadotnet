﻿using System;
using System.Collections.Generic;
using Akka.Actor;
using AkkaMovieStreamTest.Common.Messages;

namespace AkkaMovieStreamTest.Common.Actors
{
    public class UserCoordinatorActor : LogActorBase
    {
        private readonly Dictionary<int, IActorRef> _users;
        public UserCoordinatorActor()
        {
            _users = new Dictionary<int, IActorRef>();
            Receive<PlayMovieMessage>(message =>
            {
                CreateUserIfNotExist(message.UserID);
                IActorRef actorRef = _users[message.UserID];
                actorRef.Tell(message);
            });
            Receive<StopMovieMessage>(message =>
            {
                CreateUserIfNotExist(message.UserID);
                IActorRef actorRef = _users[message.UserID];
                actorRef.Tell(message);
            });
        }

        private void CreateUserIfNotExist(int userId)
        {
            if (!_users.ContainsKey(userId))
            {
                IActorRef childActorRef = Context
                    .ActorOf(Props.Create(() => new UserActor(userId)), "User" + userId);
                _users.Add(userId, childActorRef);
                Console.WriteLine("Creating User " + userId + ", Count " + _users.Count);
            }
        }
    }
}
