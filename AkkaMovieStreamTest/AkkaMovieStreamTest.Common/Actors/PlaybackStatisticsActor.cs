﻿using System;
using Akka.Actor;
using AkkaMovieStreamTest.Common.Messages;

namespace AkkaMovieStreamTest.Common.Actors
{
    public class PlaybackStatisticsActor : LogActorBase
    {
        public PlaybackStatisticsActor()
        {
            Context.ActorOf(Props.Create<MoviePlayCounterActor>(), "MoviePlayCounter");
        }

        protected override SupervisorStrategy SupervisorStrategy()
        {
            return new OneForOneStrategy(ex =>
            {
                if (ex is NotImplementedException) return Directive.Restart;
                else if (ex is NotSupportedException) return Directive.Resume;
                else return Directive.Restart;
            });
        }
    }
}
