﻿using System;
using AkkaMovieStreamTest.Common;
using Akka.Actor;

namespace AkkaMovieStreamTest.Common.Actors
{
    public abstract class LogActorBase : ReceiveActor
    {
        protected override void PreStart()
        {
            ColorConsole.CWLDarkMagenta($"{Self.Path.Name} PreStart");
            base.PreStart();
        }

        protected override void PostStop()
        {
            ColorConsole.CWLDarkMagenta($"{Self.Path.Name} PostStop");
            base.PostStop();
        }

        protected override void PreRestart(Exception reason, object message)
        {
            ColorConsole.CWLDarkMagenta($"{Self.Path.Name} PreRestart " + reason);
            base.PreRestart(reason, message);
        }

        protected override void PostRestart(Exception reason)
        {
            ColorConsole.CWLDarkMagenta($"{Self.Path.Name} PostRestart " + reason);
            base.PostRestart(reason);
        }
    }
}
