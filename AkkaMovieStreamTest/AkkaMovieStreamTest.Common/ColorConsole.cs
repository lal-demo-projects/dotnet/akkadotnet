﻿using System;

namespace AkkaMovieStreamTest.Common
{
    public static class ColorConsole
    {
        private static void WriteMessage(string message, ConsoleColor color)
        {
            Console.ForegroundColor = color;
            Console.WriteLine(message);
            Console.ForegroundColor = ConsoleColor.Gray;
        }
        public static void CWLDarkBlue(string message) => WriteMessage(message, ConsoleColor.DarkBlue);
        public static void CWLDarkGreen(string message) => WriteMessage(message, ConsoleColor.DarkGreen);
        public static void CWLDarkCyan(string message) => WriteMessage(message, ConsoleColor.DarkCyan);
        public static void CWLDarkRed(string message) => WriteMessage(message, ConsoleColor.DarkRed);
        public static void CWLDarkMagenta(string message) => WriteMessage(message, ConsoleColor.DarkMagenta);
        public static void CWLDarkYellow(string message) => WriteMessage(message, ConsoleColor.DarkYellow);
        public static void CWLGrey(string message) => WriteMessage(message, ConsoleColor.Gray);
        public static void CWLDarkGrey(string message) => WriteMessage(message, ConsoleColor.DarkGray);
        public static void CWLBlue(string message) => WriteMessage(message, ConsoleColor.Blue);
        public static void CWLGreen(string message) => WriteMessage(message, ConsoleColor.Green);
        public static void CWLCyan(string message) => WriteMessage(message, ConsoleColor.Cyan);
        public static void CWLRed(string message) => WriteMessage(message, ConsoleColor.Red);
        public static void CWLMagenta(string message) => WriteMessage(message, ConsoleColor.Magenta);
        public static void CWLYellow(string message) => WriteMessage(message, ConsoleColor.Yellow);
        public static void CWLWhite(string message) => WriteMessage(message, ConsoleColor.White);
    }
}
