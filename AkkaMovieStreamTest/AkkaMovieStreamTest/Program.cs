﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Akka.Actor;
using AkkaMovieStreamTest.Common;
using AkkaMovieStreamTest.Common.Actors;
using AkkaMovieStreamTest.Common.Messages;

namespace AkkaMovieStreamTest
{
    class Program
    {
        static ActorSystem actorSystem;
        static void Main(string[] args)
        {
            MainAsync(args).Wait();
        }
        static async Task MainAsync(string[] args)
        {
            actorSystem = ActorSystem.Create("MovieActorSystem");
            ColorConsole.CWLDarkYellow("Actor system Created");

            Test3();

            Console.WriteLine("Press enter to stop");
            Console.ReadLine();
            ColorConsole.CWLDarkYellow("Terminating actor system");
            await actorSystem.Terminate();
            Console.WriteLine("Press enter to exit");
            Console.ReadLine();
        }
        static void Test1()
        {
            Props playbackActorProps = Props.Create<PlaybackActor>();
            IActorRef playbackActorRef = actorSystem.ActorOf(playbackActorProps, "PlaybackActor");

            playbackActorRef.Tell("Harry Potter");
            playbackActorRef.Tell(300);
            playbackActorRef.Tell('c');
            playbackActorRef.Tell(new PlayMovieMessage("The 5th Wave", 2));

            playbackActorRef.Tell(PoisonPill.Instance);

            Props playbackActorV2Props = Props.Create<PlaybackActorV2>();
            IActorRef playbackActorV2Ref = actorSystem.ActorOf(playbackActorV2Props, "PlaybackActorV2");

            playbackActorV2Ref.Tell("Bloodshot");
            playbackActorV2Ref.Tell(1408);
            playbackActorV2Ref.Tell('x');
            playbackActorV2Ref.Tell(new PlayMovieMessage("Deadpool 2", 4));
        }
        static void Test2()
        {
            Props userActorProps = Props.Create<UserActor>();
            IActorRef userActorRef = actorSystem.ActorOf(userActorProps, "UserActor");

            Console.ReadKey();
            Console.WriteLine("Sending Harry Potter");
            userActorRef.Tell(new PlayMovieMessage("Harry Potter", 2));

            Console.ReadKey();
            Console.WriteLine("Sending 300");
            userActorRef.Tell(new PlayMovieMessage("300", 4));

            Console.ReadKey();
            Console.WriteLine("Sending Stop");
            userActorRef.Tell(new StopMovieMessage());

            Console.ReadKey();
            Console.WriteLine("Sending Another Stop");
            userActorRef.Tell(new StopMovieMessage());
        }
        static void Test3()
        {
            Props playbackActorV3Props = Props.Create<PlaybackActorV3>();
            IActorRef playbackActorV3Ref = actorSystem.ActorOf(playbackActorV3Props, "PlaybackActorV3");

            do
            {
                Console.WriteLine();
                Console.WriteLine("Enter command");
                string command = Console.ReadLine();

                if (command.StartsWith("play"))
                {
                    int userId = int.Parse(command.Split(',')[1]);
                    string movieTitle = command.Split(',')[2];

                    var message = new PlayMovieMessage(userId, movieTitle, 4);
                    actorSystem.ActorSelection("/user/PlaybackActorV3/UserCoordinator").Tell(message);
                }
                else if (command.StartsWith("stop"))
                {
                    int userId = int.Parse(command.Split(',')[1]);

                    var message = new StopMovieMessage(userId);
                    actorSystem.ActorSelection("/user/PlaybackActorV3/UserCoordinator").Tell(message);
                }
                else if (command.StartsWith("exit"))
                    break;

            } while (true);
        }
    }
}
